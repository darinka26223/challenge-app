import React, { useState } from 'react';
import { useCards, useDebounce } from './hooks';
import CardsList from './components/CardsList';
import Account from './components/Account';

const App = () => {
    const [account, setAccount] = useState<string>('');

    const debouncedAccount = useDebounce(account);

    const { cards, isLoading } = useCards(debouncedAccount);

    return (
        <div className="container">
            <Account account={account} setAccount={setAccount} />

            {debouncedAccount && <CardsList cards={cards} isLoading={isLoading} />}
        </div>
    );
}

export default App;
