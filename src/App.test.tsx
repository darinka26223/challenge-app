import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders App', () => {
    render(<App />);

    const title = screen.getByText(/NFTs/i);
    const input = screen.getByPlaceholderText('Enter your account name')

    expect(title).toBeInTheDocument();
    expect(input).toBeInTheDocument();
});
