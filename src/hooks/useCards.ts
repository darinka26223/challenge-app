import { useEffect, useCallback, useState } from 'react';
import { Card, CardsByCollection } from '../models';
import { API_BASE_URL } from '../constants';

const groupCardsByCollection = (cards: Card[] = []): CardsByCollection => cards
    .reduce((hash: CardsByCollection, obj: Card) => {
        if (!obj.collection.name) {
            return hash;
        }

        return ({
            ...hash,
            [obj.collection.name]: [...(hash[obj.collection.name] || []), obj],
        })
    }, {});

export const useCards = (account: string): { cards: CardsByCollection; isLoading: boolean } => {
    const [cards, setCards] = useState<CardsByCollection>({});
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const getCard = useCallback(async(): Promise<void> => {
        return await fetch(`${API_BASE_URL}/atomicassets/v1/assets?owner=${account}`)
            .then(response => response.json())
            .then(response => {
                setCards(groupCardsByCollection(response.data));
                setIsLoading(false);
            })
            .catch(error => {
                console.error(error);
                setCards({});
                setIsLoading(false);
            });
    }, [account]);

    useEffect(() => {
        if (account) {
            setIsLoading(true);
            getCard();
        }
    }, [getCard, account]);

    return { cards, isLoading };
}
