import React, { memo } from 'react';
import { Card } from '../../../../models';
import { IPFS_BASE_URL } from '../../../../constants';

interface ListItemProps {
    card: Card;
}

const ListItem = ({ card }: ListItemProps) => (
    <div className="card">
        {card?.data?.img && (
            <img
                className="image"
                src={`${IPFS_BASE_URL}/${card.data.img}`}
                alt={card.name}
            />
        )}

        <p>{card.name} {card?.template_mint && `#${card.template_mint}`}</p>
    </div>
);

export default memo(ListItem);
