import React, { Fragment, memo } from 'react';
import Loader from '../Loader';
import { CardsByCollection } from '../../models';
import ListItem from './components/ListItem';

interface CardsListProps {
    cards: CardsByCollection;
    isLoading: boolean,
}

const CardsList = ({ cards, isLoading }: CardsListProps) => {
    if (isLoading) {
        return <Loader />;
    }

    if (Object.keys(cards).length === 0) {
        return <p>No cards available</p>;
    }

    return (
        <Fragment>
            <h2 className="title">
                Results
            </h2>

            {Object.entries(cards).map(([key, value]) => (
                <Fragment key={key}>
                    <h3 className="title">
                        {key}
                    </h3>

                    <div className="cards-container">
                        {value.map((card) => <ListItem key={card.asset_id} card={card} />)}
                    </div>
                </Fragment>
            ))}
        </Fragment>
    )
};

export default memo(CardsList);
