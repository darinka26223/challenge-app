import React, { SetStateAction, Fragment, memo, useCallback } from 'react';

interface AccountProps {
    account: string;
    setAccount: React.Dispatch<SetStateAction<string>>,
}

const Account = ({ account, setAccount }: AccountProps) => {
    const onAccountChange = useCallback<React.ChangeEventHandler<HTMLInputElement>>(
        (event) => setAccount(event.target.value),
        [setAccount]);

    return (
        <Fragment>
            <h2 className="title">
                NFTs
            </h2>

            <input
                placeholder="Enter your account name"
                className="input"
                type="text"
                value={account}
                onChange={onAccountChange}
            />
        </Fragment>
    )
};

export default memo(Account);
