export type Card = {
    asset_id: string;
    collection: {
        name: string;
    },
    data: {
        img: string;
    },
    name: string;
    template_mint: string;
};

export type CardsByCollection = {
    [key: string]: Card[];
};
